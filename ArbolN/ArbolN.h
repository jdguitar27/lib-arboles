#ifndef ARBOLN_H
#define ARBOLN_H

#include "NodoA.h"
#include <list>
#include <string>

using std::string;
using std::list;

template <class Tipo>
class ArbolN
{
    protected:
        NodoA<Tipo> *raiz;
        NodoA<Tipo> *copiarnodos( NodoA<Tipo> *);
        void hijos(NodoA<Tipo> *, list<ArbolN<Tipo>>&);
        void destruirnodos(NodoA<Tipo> *);
        void insertar(NodoA<Tipo> *,const Tipo &,const Tipo &, bool &);
        void recorridoNiveles(list<Tipo> &);
        void recorridoPreorden(NodoA<Tipo> *, list<Tipo> &);
        void recorridoPostorden(NodoA<Tipo> *, list<Tipo> &);
    public:
        ArbolN(){this->raiz = NULL;};
        ArbolN(Tipo e){this->raiz = new NodoA<Tipo>(e);};
        ArbolN(Tipo, list<ArbolN<Tipo>>);
        ArbolN(const ArbolN<Tipo> &ar){this->raiz = copiarnodos(ar.raiz);};
        ~ArbolN(){this->Vaciar();};
        void Copiar(const ArbolN<Tipo> &ar);
        bool esNulo(){return this->raiz == NULL;};
        Tipo Raiz(){return this->raiz->obtInfo();};
        list<ArbolN<Tipo>> Hijos();
        void Vaciar();
        bool esHoja()const{return this->raiz->obtHi()==NULL;};
        void InsetarElemento(const Tipo &e){this->raiz = new NodoA<Tipo>(e);};
        void InsetarElemento(const Tipo &padre,const Tipo &e){bool in = false;this->insertar(this->raiz, padre, e, in);};
        list<Tipo> Niveles();
        list<Tipo> Preorden();
        list<Tipo> Postorden();
};

template <class Tipo>
NodoA<Tipo> * ArbolN<Tipo>::copiarnodos(NodoA<Tipo> *p)
{
    NodoA<Tipo> *aux = NULL;
    if(p != NULL)
    {
        aux = new NodoA<Tipo>(p->obtInfo(), copiarnodos(p->obtHi()), copiarnodos(p->obtHd()));
    }
    return aux;
}

template <class Tipo>
ArbolN<Tipo>::ArbolN(Tipo e, list<ArbolN<Tipo>> hijos)
{
    NodoA<Tipo> *aux;

    this->raiz = new NodoA<Tipo>(e);
    if(!hijos.empty())
    {
        this->raiz->modHi(copiarnodos(hijos.front().raiz));
        hijos.pop_front();
        aux = this->raiz->obtHi();

        while(!hijos.empty())
        {
            aux->modHd(copiarnodos(hijos.front().raiz));
            hijos.pop_front();
            aux = aux->obtHd();
        }
    }
}

template <class Tipo>
void ArbolN<Tipo>::Copiar(const ArbolN<Tipo> &ar)
{
    this->Vaciar();
    this->raiz = copiarnodos(ar.raiz);
}

template <class Tipo>
void ArbolN<Tipo>::hijos(NodoA<Tipo> *arb, list<ArbolN<Tipo>> &hi)
{

    ArbolN<Tipo>araux;
    NodoA<Tipo> *aux = arb->obtHi();

    while(aux != NULL)
    {
        araux.raiz = copiarnodos(aux);
        hi.push_back(araux);
        aux = aux->obtHd();
    }
}

template <class Tipo>
list<ArbolN<Tipo>> ArbolN<Tipo>::Hijos()
{
    list<ArbolN<Tipo>> hi;
    if(this->raiz != NULL)
        this->hijos(this->raiz, hi);
    return hi;
}

template <class Tipo>
void ArbolN<Tipo>::destruirnodos(NodoA<Tipo> *arb)
{
    if(arb != NULL)
    {
        if(arb->obtHd() != NULL)
        {
            this->destruirnodos(arb->obtHd());
        }
        if(arb->obtHi() != NULL)
        {
            this->destruirnodos(arb->obtHi());
        }
        delete arb;
        arb = NULL;
    }
}

template <class Tipo>
void ArbolN<Tipo>::Vaciar()
{
    if(this->raiz != NULL)
    {
        this->destruirnodos(this->raiz);
        this->raiz = NULL;
    }
}

template <class Tipo>
void ArbolN<Tipo>::insertar(NodoA<Tipo> *arb,const Tipo &padre,const Tipo &e, bool &insertado)
{
    NodoA<Tipo> *aux = arb;
    if(aux != NULL && !insertado)
    {
        if(aux->obtInfo() == padre)
        {
            if(aux->obtHi() == NULL)
                aux->modHi(new NodoA<Tipo>(e));
            else
            {
                aux = aux->obtHi();
                while(aux->obtHd() != NULL)
                    aux = aux->obtHd();
                aux->modHd(new NodoA<Tipo>(e));
            }
            insertado = true;
        }
        else
        {
            this->insertar(aux->obtHi(), padre, e, insertado);
            this->insertar(aux->obtHd(), padre, e, insertado);
        }
    }
}

template <class Tipo>
void ArbolN<Tipo>::recorridoNiveles(list<Tipo> &recorrido)
{
    list<NodoA<Tipo> *> hijos;
    NodoA<Tipo> *aux;

    hijos.push_back(this->raiz);
    recorrido.push_back(this->raiz->obtInfo());
    while(!hijos.empty())
    {
        aux = hijos.front()->obtHi();
        while(aux != NULL)
        {
            hijos.push_back(aux);
            recorrido.push_back(aux->obtInfo());
            aux = aux->obtHd();
        }
        hijos.pop_front();
    }
}

template <class Tipo>
list<Tipo> ArbolN<Tipo>::Niveles()
{
    list<Tipo> niveles;
    if(this->raiz != NULL)
        this->recorridoNiveles(niveles);
    return niveles;
}

template <class Tipo>
void ArbolN<Tipo>::recorridoPreorden(NodoA<Tipo> *raiz, list<Tipo> &preorden)
{
    if(raiz != NULL)
    {
        preorden.push_back(raiz->obtInfo());
        recorridoPreorden(raiz->obtHi(), preorden);
        recorridoPreorden(raiz->obtHd(), preorden);
    }
}

template <class Tipo>
list<Tipo> ArbolN<Tipo>::Preorden()
{
    list<Tipo> preorden;

    this->recorridoPreorden(this->raiz, preorden);

    return preorden;
}

template <class Tipo>
void ArbolN<Tipo>::recorridoPostorden(NodoA<Tipo> *raiz, list<Tipo> &post)
{
    if(raiz != NULL)
    {
        recorridoPostorden(raiz->obtHi(), post);
        post.push_back(raiz->obtInfo());
        recorridoPostorden(raiz->obtHd(), post);
    }
}

template <class Tipo>
list<Tipo> ArbolN<Tipo>::Postorden()
{
    list<Tipo> post;

    this->recorridoPostorden(this->raiz, post);

    return post;
}


#endif