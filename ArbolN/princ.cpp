#include <iostream>
#include "ArbolN.h"
using namespace std;
int main()
{
    list<char> niveles, preorden, postorden, inorden;
    ArbolN<char> arb;
    char padre, hijo;
    int n;
    cin >> padre >> n;

    arb.InsetarElemento(padre);
    for(int i = 0; i < n; i++)
    {
        cin >> padre >> hijo;
        arb.InsetarElemento(padre, hijo);
    }
    niveles = arb.Niveles();
    preorden = arb.Preorden();
    postorden = arb.Postorden();
    
    return 0;
}