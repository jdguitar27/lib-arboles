#include <iostream>
#include "ABB.h"
using std::cout;
using std::endl;
int main()
{
    int e;
    list<int> preorden, postorden, inorden, niveles;
    ABB<int> aux2;

    for(int i=1; i <= 10 ; i++)
    {
        std::cin >> e;
        aux2.Insertar(e);
    }
    preorden = aux2.Preorden();
    postorden = aux2.Postorden();
    inorden = aux2.Inorden();
    niveles = aux2.Niveles();
    aux2.Vaciar();
    return 0;
}