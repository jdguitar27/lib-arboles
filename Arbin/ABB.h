#ifndef ABB_H
#define ABB_H

#include "Arbin.h"

template <class Tipo>
class ABB  : public Arbin<Tipo>
{
    protected:
        NodoA<Tipo> *insertarElemento(NodoA<Tipo> *, Tipo);
    public:
        ABB(){this->raiz = NULL;};
        ABB(Tipo e){this->raiz = new NodoA<Tipo>(e);};
        ABB(Tipo , ABB<Tipo> , ABB<Tipo> );
        ABB(const ABB<Tipo> &ar){this->raiz = this->copiarnodos(ar.raiz);};
        ~ABB(){this->Vaciar();};
        void Insertar(Tipo);
};

template <class Tipo>
ABB<Tipo>::ABB(Tipo e, ABB<Tipo> izq, ABB<Tipo> der)
{
    this->raiz = new NodoA<Tipo>(e, this->copiarnodos(izq.raiz), this->copiarnodos(der.raiz));
}

template <class Tipo>
void ABB<Tipo>::Insertar(Tipo e)
{       
    this->raiz = this->insertarElemento(this->raiz, e);
}

template <class Tipo>
NodoA<Tipo> *ABB<Tipo>::insertarElemento(NodoA<Tipo> *arb, Tipo e)
{
    if(arb == NULL)
        arb = new NodoA<Tipo>(e);
    else
        e >= arb->obtInfo()? arb->modHd(this->insertarElemento(arb->obtHd(), e)):arb->modHi(this->insertarElemento(arb->obtHi(), e));
    return arb;    
}
#endif