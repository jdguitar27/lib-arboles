#include <iostream>
#include "Arbin.h"

using std::cout;
using std::cin;
using std::endl;

void construir_arbol(Arbin<int> &arb)
{
    int n, e;
    list<int> recorrido, inorden;
    string tipo, tipo2;
    cin >> tipo >> n;
        for(int j = 0; j < n ; j++)
        {
            cin >> e;
            recorrido.push_back(e);
        }
        cin >> tipo2 >> n;
        for(int j = 0; j < n ; j++)
        {
            cin >> e;
            inorden.push_back(e);
        }
        if(tipo == "PREORDEN")
            arb.Construir_Pre_In(recorrido , inorden);
        else if(tipo == "POSTORDEN")
            arb.Construir_Post_In(recorrido, inorden);

        recorrido.clear();
        inorden.clear(); 
}

int main()
{
    Arbin<int> b1, b2;

    for(int i = 1 ; i <= 2; i++)
    {
        construir_arbol(b1);
        construir_arbol(b2);
        if(b1.Mayor() < b2.Menor())
            cout << "Es menor\n";
        else
            cout << "No es menor\n";
        b1.Vaciar();
        b2.Vaciar();
    }

    return 0;
}