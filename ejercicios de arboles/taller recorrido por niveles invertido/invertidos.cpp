#include <iostream>
#include "Arbin.h"

using std::cout;
using std::cin;
using std::endl;

void Leer(list<char> &l, const int &n)
{
    char e;

    for(int i = 0; i < n; i++)
    {
        cin >> e;
        l.push_back(e);
    }
}

int main()
{
    Arbin<char> arb;
    int casos, n;
    string tipo;
    list<char> preorden, postorden, inorden, niveles;
    cin >> casos;

    for(int i = 0; i < casos; i++)
    {
        cin >> n;
        for( int k = 0 ; k < 2; k++)
        {
            cin >> tipo;
            if(tipo == "PREORDEN")
                Leer(preorden,n);
            else if(tipo == "POSTORDEN")
                Leer(postorden,n);
            else
                Leer(inorden,n);
        }
        
        !preorden.empty()?arb.Construir_Pre_In(preorden, inorden):arb.Construir_Post_In(postorden,inorden);

        niveles = arb.Niveles_invertido();

        cout << "Caso #" << i+1 << ": ";
        for(list<char>::iterator k = niveles.begin(); k != niveles.end(); k++)
            cout << *k << " ";
        cout <<endl;

        preorden.clear();
        postorden.clear();
        inorden.clear();
        niveles.clear();
        arb.Vaciar();
    }
    return 0;
}