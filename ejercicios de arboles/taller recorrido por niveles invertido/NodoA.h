#ifndef NODOA_H
#define NODOA_H

template <class Tipo>
class NodoA
{
    private:
        Tipo info;
        NodoA *hd, *hi;
    public:
        NodoA(){this->hi = NULL; this->hd = NULL;};
        NodoA(Tipo e){this->info = e; this->hi = NULL; this->hd = NULL;};
        NodoA(Tipo e, NodoA *izq, NodoA *der){this->hi = izq; this->hd = der; this->info = e;};
        ~NodoA(){this->hi = NULL; this->hd = NULL;};
        void modInfo(Tipo e){this->info = e;};
        void modHi(NodoA *p){this->hi = p;};
        void modHd(NodoA *p){this->hd = p;};
        Tipo obtInfo()const {return this->info;};
        NodoA * obtHi()const {return this->hi;};
        NodoA * obtHd()const {return this->hd;};
};
#endif