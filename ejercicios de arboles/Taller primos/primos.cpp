#include <iostream>
#include "ArbolN.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
    ArbolN<string> arb;
    string padre, hijo, buscar;
    int n;
    list<string> prim;

    while(cin >> padre)
    {
        if(padre != "***")
        {
            if(arb.esNulo())
                arb.InsetarElemento(padre);    
            cin >> n;
            for(int i = 0; i < n; i++)
            {
                cin >> hijo;
                arb.InsetarElemento(padre, hijo);
            }
        }
        else
        {
            cin >> buscar;
            prim = arb.Primos(buscar);
            arb.Vaciar();
        }
    }
    
    return 0;
}