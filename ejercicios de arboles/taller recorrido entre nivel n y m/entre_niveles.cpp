//José Durán 26389333
#include <iostream>
#include "Arbin.h"

using std::cout;
using std::cin;
using std::endl;

void Leer_entrada(Arbin<char> &arb, int &n, int &m, list<char> &recorrido, list<char> &inorden);

int main()
{
    int casos, n, m;
    list<char> recorrido, inorden, niveles;
    Arbin<char> arb;
    list<char>::iterator fin;
    cin >> casos;
    for(int i = 1; i <= casos; i++)
    {
        Leer_entrada(arb, n, m, recorrido, inorden);
        niveles = arb.Entre_Niveles(n,m);
        fin = niveles.end();
        fin--;
        cout << "Caso #"<< i << ": ";
        for(list<char>::iterator k = niveles.begin(); k != niveles.end(); k++)
            k != fin? cout << *k<<" ": cout << *k << endl;
        recorrido.clear();
        inorden.clear();
        niveles.clear();
        arb.Vaciar();
    }
    return 0;
}

void Leer_entrada(Arbin<char> &arb, int &n, int &m, list<char> &recorrido, list<char> &inorden)
{
    int cant;
    char e;
    string tipo, tipo2;
    cin >> cant >> n >> m;
    cin >> tipo;
    for(int j = 0; j < cant ; j++)
    {
        cin >> e;
        recorrido.push_back(e);
    }
    cin >> tipo2;
    for(int j = 0; j < cant ; j++)
    {
        cin >> e;
        inorden.push_back(e);
    }
    if(tipo == "PREORDEN")
        arb.Construir_Pre_In(recorrido , inorden);
    else if(tipo == "POSTORDEN")
        arb.Construir_Post_In(recorrido, inorden);
}