#include <iostream>
#include "ArbolN.h"

using std::cout;
using std::cin;
using std::endl;

void frontera(ArbolN<char> arb, list<char> &f)
{
    list<ArbolN<char>> hijos;
    if(arb.esHoja())
        f.push_back(arb.Raiz());
    else
    {
        hijos = arb.Hijos();
        while(!hijos.empty())
        {
            frontera(hijos.front(), f);
            hijos.pop_front();
        }
    }
}
int main()
{
    ArbolN<char> arb;
    char padre, hijo;
    int n;
    list<char> f;
    cin >> padre >> n;

    arb.InsetarElemento(padre);
    for(int i = 0; i < n; i++)
    {
        cin >> padre >> hijo;
        arb.InsetarElemento(padre, hijo);
    }
    frontera(arb, f);
    for(list<char>::iterator k = f.begin(); k != f.end(); k++)
        cout << *k << " ";
    cout << endl;
    arb.Vaciar();
    return 0;
}