//José Durán 26389333
#include <iostream>
#include "Arbin.h"

using std::cout;
using std::cin;
using std::endl;

void Leer(list<int> &l)
{
    int e, n;
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        cin >> e;
        l.push_back(e);
    }
}

int main()
{
    string tipo;
    Arbin<int> arb;
    list<int> inorden, preorden, postorden;

    for(int i = 1 ; i <= 2; i++)
    {
        for(int j = 1 ; j <= 2; j++)
        {
            cin >> tipo;
            if(tipo == "PREORDEN")
                Leer(preorden);
            else if(tipo == "POSTORDEN")
                Leer(postorden);
            else
                Leer(inorden);
        }

        !preorden.empty()?arb.Construir_Pre_In(preorden,inorden):arb.Construir_Post_In(postorden,inorden);
        
        arb.esCompleto()?cout << "ES completo.\n":cout << "NO es completo.\n";

        preorden.clear();
        postorden.clear();
        inorden.clear();
        arb.Vaciar();          
    }
    return 0;
}