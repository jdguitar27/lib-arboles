//José Durán 26389333
#include <iostream>
#include <string>
#include "Arbin.h"

using std::cin;
using std::cout;
using std::string;
using std::endl;

int main()
{
    int n;
    string pre;
    Arbin<char> arb;
    list<char> inorden;

    cin >> n;

    for(int i = 0; i < n; i++)
    {
        cin >> pre;
        arb.Construir_Preorden(pre);
        inorden = arb.Inorden();
        for(list<char>::iterator j = inorden.begin(); j != inorden.end() ; j++)
            cout << *j;
        cout << endl;
        arb.Vaciar();
    }

    return 0;
}