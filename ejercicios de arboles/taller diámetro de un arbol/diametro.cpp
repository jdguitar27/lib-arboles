//José Durán 26389333
#include <iostream>
#include "Arbin.h"

using std::cout;
using std::cin;
using std::endl;

void Leer(list<int> &l, const int &n)
{
    int e;
    for(int i = 0; i < n; i++)
    {
        cin >> e;
        l.push_back(e);
    }
}

int main()
{
    int n;
    string tipo;
    Arbin<int> arb;
    list<int> inorden, preorden, postorden;

    for(int i = 1 ; i <= 2; i++)
    {
        cin >> n;
        for(int j = 1 ; j <= 2; j++)
        {
            cin >> tipo;
            if(tipo == "PREORDEN")
                Leer(preorden,n);
            else if(tipo == "POSTORDEN")
                Leer(postorden,n);
            else
                Leer(inorden,n);
        }

        !preorden.empty()?arb.Construir_Pre_In(preorden,inorden):arb.Construir_Post_In(postorden,inorden);

        cout <<"El diametro del arbol #"<< i <<" es " << arb.Diametro()<<"."<<endl;

        preorden.clear();
        postorden.clear();
        inorden.clear();
        arb.Vaciar();          
    }
    return 0;
}