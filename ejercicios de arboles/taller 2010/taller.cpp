//José Durán 26389333
#include <iostream>
#include <string>
#include "Arbin.h"

using std::cin;
using std::cout;
using std::string;
using std::endl;

int main()
{
    int n;
    string post;
    Arbin<char> arb;
    list<char> inorden;

    cin >> n;

    for(int i = 0; i < n; i++)
    {
        cin >> post;
        arb.Construir_Postorden(post);
        inorden = arb.Inorden();
        for(list<char>::iterator j = inorden.begin(); j != inorden.end() ; j++)
            cout << *j;
        cout << endl;
        arb.Vaciar();
    }

    return 0;
}