#ifndef ARBIN_H
#define ARBIN_H

#include "NodoA.h"
#include <list>
#include <string>

using std::list;
using std::string;

template <class Tipo>
class Arbin
{
    protected:
        NodoA<Tipo> *raiz;
        NodoA<Tipo> *copiarnodos( NodoA<Tipo> *);
        void recorridoPreorden(NodoA<Tipo> *, list<Tipo> &);
        void recorridoPostorden(NodoA<Tipo> *, list<Tipo> &);
        void recorridoInorden(NodoA<Tipo> *, list<Tipo> &);
        void recorridoNiveles(list<Tipo> &);
        NodoA<Tipo> *Construir_pre_in(list<Tipo> &pre, list<Tipo> &in);
        NodoA<Tipo> *Construir_post_in(list<Tipo> &post, list<Tipo> &in);
        NodoA<Tipo> *Construir_post(list<Tipo> &);
        NodoA<Tipo> *Construir_pre(list<Tipo> &);
        void buscarmenor(Tipo &, NodoA<Tipo> *);
        void buscarmayor(Tipo &, NodoA<Tipo> *);
        int altura(NodoA<Tipo> *);
        void recorridoentreNiveles(list<Tipo> &, int, int);
        int nivelDelNodo(NodoA<Tipo> *, NodoA<Tipo> *, int );
        void vaciarArbol(NodoA<Tipo> *);
    public:
        Arbin(){this->raiz = NULL;};
        Arbin(Tipo e){this->raiz = new NodoA<Tipo>(e);};
        Arbin(Tipo , Arbin<Tipo> , Arbin<Tipo> );
        Arbin(const Arbin<Tipo> &ar){this->raiz = copiarnodos(ar.raiz);};
        ~Arbin(){this->Vaciar();};
        Tipo Raiz(){return this->raiz->obtInfo();};
        void Copiar(const Arbin<Tipo> &);
        bool esNulo()const{return this->raiz == NULL;};
        Arbin<Tipo> HijoIzquierdo();
        Arbin<Tipo> HijoDerecho();
        list<Tipo> Preorden();
        list<Tipo> Postorden();
        list<Tipo> Inorden();
        list<Tipo> Niveles();
        void Construir_Pre_In(list<Tipo> ,list<Tipo> );
        void Construir_Post_In(list<Tipo> ,list<Tipo> );
        void Construir_Postorden(string );
        void Construir_Preorden(string );
        Tipo Menor();
        Tipo Mayor();
        int Altura();
        list<Tipo> Entre_Niveles(int n, int m);
        void Vaciar();
};

template <class Tipo>
NodoA<Tipo> * Arbin<Tipo>::copiarnodos(NodoA<Tipo> *p)
{
    NodoA<Tipo> *aux = NULL;
    if(p != NULL)
    {
        aux = new NodoA<Tipo>(p->obtInfo(), copiarnodos(p->obtHi()), copiarnodos(p->obtHd()));
    }
    return aux;
}

template <class Tipo>
Arbin<Tipo>::Arbin(Tipo e, Arbin<Tipo> izq, Arbin<Tipo> der)
{
    this->raiz = new NodoA<Tipo>(e, this->copiarnodos(izq.raiz), this->copiarnodos(der.raiz));
}

template <class Tipo>
void Arbin<Tipo>::Copiar(const Arbin<Tipo> &ar)
{
    this->raiz = copiarnodos(ar.raiz);
}

template <class Tipo>
Arbin<Tipo> Arbin<Tipo>::HijoIzquierdo()
{
    Arbin<Tipo> hijo;

    hijo.raiz = copiarnodos(this->raiz->obtHi());

    return hijo;
}

template <class Tipo>
Arbin<Tipo> Arbin<Tipo>::HijoDerecho()
{
    Arbin<Tipo> hijo;

    hijo.raiz = copiarnodos(this->raiz->obtHd());

    return hijo;
}

template <class Tipo>
void Arbin<Tipo>::recorridoPreorden(NodoA<Tipo> *raiz, list<Tipo> &preorden)
{
    if(raiz != NULL)
    {
        preorden.push_back(raiz->obtInfo());
        recorridoPreorden(raiz->obtHi(), preorden);
        recorridoPreorden(raiz->obtHd(), preorden);
    }
}

template <class Tipo>
list<Tipo> Arbin<Tipo>::Preorden()
{
    list<Tipo> preorden;

    this->recorridoPreorden(this->raiz, preorden);

    return preorden;
}

template <class Tipo>
void Arbin<Tipo>::recorridoPostorden(NodoA<Tipo> *raiz, list<Tipo> &post)
{
    if(raiz != NULL)
    {
        recorridoPostorden(raiz->obtHi(), post);
        recorridoPostorden(raiz->obtHd(), post);
        post.push_back(raiz->obtInfo());
    }
}

template <class Tipo>
list<Tipo> Arbin<Tipo>::Postorden()
{
    list<Tipo> post;

    this->recorridoPostorden(this->raiz, post);

    return post;
}

template <class Tipo>
void Arbin<Tipo>::recorridoInorden(NodoA<Tipo> *raiz, list<Tipo> &inorden)
{
    if(raiz != NULL)
    {
        recorridoInorden(raiz->obtHi(), inorden);
        inorden.push_back(raiz->obtInfo());
        recorridoInorden(raiz->obtHd(), inorden);
    }
}

template <class Tipo>
list<Tipo> Arbin<Tipo>::Inorden()
{
    list<Tipo> inorden;

    this->recorridoInorden(this->raiz, inorden);

    return inorden;
}

template <class Tipo>
void Arbin<Tipo>::recorridoNiveles(list<Tipo> &niveles)
{
    list<NodoA<Tipo> *> hijos;
    NodoA<Tipo> *aux;

    hijos.push_back(this->raiz);
    niveles.push_back(this->raiz->obtInfo());
    aux = this->raiz;
    while(!hijos.empty())
    {
        if(aux->obtHi() != NULL)
        {
            hijos.push_back(aux->obtHi());
        }
        if(aux->obtHd() != NULL)
        {
            hijos.push_back(aux->obtHd());
        }
        hijos.pop_front();
        if(!hijos.empty())
        {
            aux = hijos.front();
            niveles.push_back(aux->obtInfo());
        }
    }
}

template <class Tipo>
list<Tipo> Arbin<Tipo>::Niveles()
{
    list<Tipo> niveles;
    recorridoNiveles(niveles);
    return niveles;
}

template <class Tipo>
NodoA<Tipo> *Arbin<Tipo>::Construir_pre_in(list<Tipo> &pre, list<Tipo> &in)
{
    NodoA<Tipo> *arb=NULL;
    list<Tipo> izq;

    if(!pre.empty() && !in.empty())
    {
        while(pre.front()!= in.front())
        {
            izq.push_back(in.front());
            in.pop_front();
        }
        arb = new NodoA<Tipo>(pre.front());
        pre.pop_front();
        in.pop_front();
        arb->modHi(Construir_pre_in(pre, izq));
        arb->modHd(Construir_pre_in(pre, in));
    }
    return arb;
}

template <class Tipo>
void Arbin<Tipo>::Construir_Pre_In(list<Tipo> pre, list<Tipo> in)
{
    this->raiz = Construir_pre_in(pre, in);
}

template <class Tipo>
NodoA<Tipo> *Arbin<Tipo>::Construir_post_in(list<Tipo> &post, list<Tipo> &in)
{
    NodoA<Tipo> *arb=NULL;
    list<Tipo> izq;

    if(!post.empty() && !in.empty())
    {
        while(post.back()!= in.front())
        {
            izq.push_back(in.front());
            in.pop_front();
        }
        arb = new NodoA<Tipo>(post.back());
        post.pop_back();
        in.pop_front();
        arb->modHd(Construir_post_in(post, in));
        arb->modHi(Construir_post_in(post, izq));
    }
    return arb;
}

template <class Tipo>
void Arbin<Tipo>::Construir_Post_In(list<Tipo> post, list<Tipo> in)
{
    this->raiz = Construir_post_in(post, in);
}

template <class Tipo>
NodoA<Tipo> *Arbin<Tipo>::Construir_post(list<Tipo> &post)
{
    NodoA<Tipo> *arb = NULL;

    if(!post.empty())
    {
        arb = new NodoA<Tipo>(post.back());
        post.pop_back();
        if( arb->obtInfo() >= 65 && arb->obtInfo() <= 90)
        {
            arb->modHd(Construir_post(post));
            arb->modHi(Construir_post(post));
        }
    }
    return arb;
}

template <class Tipo>
void Arbin<Tipo>::Construir_Postorden(string entrada)//Construye usando recorrido en postorden para arbol de caracteres con exactamente 2 hijos en los nodos internos
{
    list<Tipo> post;
    while(!entrada.empty())
    {
        post.push_back(entrada[0]);
        entrada.erase(0,1);
    }
    this->raiz = Construir_post(post);
}

template <class Tipo>
NodoA<Tipo> *Arbin<Tipo>::Construir_pre(list<Tipo> &pre)
{
    NodoA<Tipo> *arb = NULL;

    if(!pre.empty())
    {
        arb = new NodoA<Tipo>(pre.front());
        pre.pop_front();
        if( arb->obtInfo() >= 65 && arb->obtInfo() <= 90)
        {
            arb->modHi(Construir_pre(pre));
            arb->modHd(Construir_pre(pre));
        }
    }
    return arb;
}

template <class Tipo>
void Arbin<Tipo>::Construir_Preorden(string entrada)//Construye usando recorrido en preorden para arbol de caracteres con exactamente 2 hijos en los nodos internos
{
    list<Tipo> pre;
    while(!entrada.empty())
    {
        pre.push_back(entrada[0]);
        entrada.erase(0,1);
    }
    this->raiz = Construir_pre(pre);
}

template <class Tipo>
void Arbin<Tipo>::buscarmenor(Tipo &menor, NodoA<Tipo> *arb)
{
    Tipo izq = menor, der = menor;
    if(arb != NULL)
    {
        buscarmenor(izq, arb->obtHi());
        buscarmenor(der, arb->obtHd());
        if(izq < menor)
            menor = izq;
        if(der < menor)
            menor = der;
        if(arb->obtInfo() < menor)
            menor = arb->obtInfo();
    }
}

template <class Tipo>
Tipo Arbin<Tipo>::Menor()
{
    Tipo menor;

    menor = this->raiz->obtInfo();
    buscarmenor(menor, this->raiz);
    return menor;
}

template <class Tipo>
void Arbin<Tipo>::buscarmayor(Tipo &mayor, NodoA<Tipo> *arb)
{
    Tipo izq = mayor, der = mayor;
    if(arb != NULL)
    {
        buscarmayor(izq, arb->obtHi());
        buscarmayor(der, arb->obtHd());
        if(izq > mayor)
            mayor = izq;
        if(der > mayor)
            mayor = der;
        if(arb->obtInfo() > mayor)
            mayor = arb->obtInfo();
    }
}

template <class Tipo>
Tipo Arbin<Tipo>::Mayor()
{
    Tipo mayor;

    mayor = this->raiz->obtInfo();
    buscarmayor(mayor, this->raiz);
    return mayor;
}

template <class Tipo>
int Arbin<Tipo>::altura(NodoA<Tipo> *arb)
{
    int res = -1, izq, der;

    if(arb != NULL)
    {
        izq = altura(arb->obtHi());
        der = altura(arb->obtHd());
        izq >= der ? res = izq+1 : res = der+1;
    }

    return res;
}

template <class Tipo>
int Arbin<Tipo>::Altura()
{
    return altura(this->raiz);
}

template <class Tipo>
void Arbin<Tipo>::recorridoentreNiveles(list<Tipo> &niveles, int n, int m)
{
    list<NodoA<Tipo> *> hijos;
    NodoA<Tipo> *aux;
    int nivel;

    hijos.push_back(this->raiz);
    aux = this->raiz;

    if(n == 0)
        niveles.push_back(aux->obtInfo());

    while(!hijos.empty())
    {
        if(aux->obtHi() != NULL)
        {
            hijos.push_back(aux->obtHi());
        }
        if(aux->obtHd() != NULL)
        {
            hijos.push_back(aux->obtHd());
        }
        hijos.pop_front();
        if(!hijos.empty())
        {
            aux = hijos.front();
            nivel = nivelDelNodo(this->raiz,aux, 0);
            if(nivel >= n && nivel <= m)
                niveles.push_back(aux->obtInfo());
        }
    }
}

template <class Tipo>
list<Tipo> Arbin<Tipo>::Entre_Niveles(int n, int m)
{
    list<Tipo> niveles;
    //if(n >= 0 && m <= altura(this->raiz)); //Si no se garantiza la consulta de solo niveles existentes, activar línea
        this->recorridoentreNiveles(niveles, n, m);
    return niveles;
}

template <class Tipo>
int Arbin<Tipo>::nivelDelNodo(NodoA<Tipo> *aux, NodoA<Tipo> *arb, int i)
{
    int nivel = -1, izq, der;
    if(aux == arb || aux == NULL)
    {
        if(aux != NULL)
            nivel = i;
    }
    else
    {
        izq = nivelDelNodo(aux->obtHi(), arb, i+1);
        der = nivelDelNodo(aux->obtHd(), arb, i+1);
        if(izq != -1)
            nivel = izq;
        else if(der != -1)
            nivel = der;
    }
    return nivel;
}

template <class Tipo>
void Arbin<Tipo>::vaciarArbol(NodoA<Tipo> *arb)
{
    if(arb->obtHi()!=NULL)
    {
        vaciarArbol(arb->obtHi());
    }
    if(arb->obtHd()!=NULL)
    {
        vaciarArbol(arb->obtHd());
    }
    delete arb;
    arb = NULL;
}

template <class Tipo>
void Arbin<Tipo>::Vaciar()
{
    if(this->raiz != NULL)
    {
        vaciarArbol(this->raiz);
        this->raiz = NULL;
    }
}
#endif